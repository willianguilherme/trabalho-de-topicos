class Requisicao {
    constructor(link, params){
        this.link = link
        this.params = params
        this.url = "https://app.professordaniloalves.com.br/api/v1"
    }

    async enviarReq() {
      const json = {
        peso: "70.2",
        altura: "1.71"
      };

      const options = {
        method: 'POST',
        body: JSON.stringify(json),
        headers: {
            'Content-Type': 'application/json'
        }
      }

      await fetch(this.url + '/imc/calcular', options)
        .then(res => res.json())
        .then(res => console.log(res))
        .catch(err => console.error(err));
    }
}

export { Requisicao }
