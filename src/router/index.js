import { createRouter, createWebHistory } from "vue-router";
import Imc from "../components/organisms/Imc.vue";
import Home from "../components/organisms/Home.vue";
import QuemSomos from "../components/organisms/QuemSomos.vue";
import Cadastro from "../components/organisms/Cadastro.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
    },
    {
      path: "/imc",
      name: "imc",
      component: Imc,
    },
    {
      path: "/cadastro",
      name: "cadastro",
      component: Cadastro,
    },
    {
      path: "/quem-somos",
      name: "quem-somos",
      component: QuemSomos,
    },
  ],
});

export default router;
