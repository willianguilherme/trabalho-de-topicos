import { createApp } from "/node_modules/.vite/vue.js?v=be1f77ce";


import "/src/assets/css/base.css";
import App from "/src/App.vue";
import router from "/src/router/index.js?t=1647955191965";

const app = createApp(App);

app.use(router);
app.mount("#app");
