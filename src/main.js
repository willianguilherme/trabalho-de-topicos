import { createApp } from "vue";


import "./assets/css/base.css";
import App from "./App.vue";
import router from "./router";

const app = createApp(App);

app.use(router);
app.mount("#app");

localStorage.setItem("url", "https://app.professordaniloalves.com.br/api/v1/")
